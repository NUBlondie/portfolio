Portfolio::Application.routes.draw do
  root "welcome#index"

  get '/challenge' => 'welcome#challenge'

end
